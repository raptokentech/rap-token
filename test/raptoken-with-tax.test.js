const HRE = require('hardhat');
const chai = require("chai");

const CHARITY_WALLET_1 = "0xF18e8c594d25De814c2e432A088a7D76BE9B220C";
const CHARITY_WALLET_2 = "0x147537499Ca7844B74Ba26Aae5da08f9a8DbB854";
const ADMIN_WALLET = "0x3c021c3999B9f011Ff5a48b2F4B3f1802401699F";
const BURN_ADDRESS = "0x000000000000000000000000000000000000dEaD";
const panCakeV2RouterAddress = "0xD99D1c33F9fC3444f8101754aBC46c52416550D1";

const DECIMAL_ZEROS = "000000000000000000"; // 18 zeros

const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";

describe("RapToken with tax tests", function() {
  beforeEach(async function() {
    this.users = await ethers.getSigners();
    const tenEther = ethers.utils.parseEther('50');
    const RapToken = await ethers.getContractFactory("RapToken");
    this.rapToken = await RapToken.deploy();
    this.panCakeRouter = await ethers.getContractAt("IPancakeV2Router02", panCakeV2RouterAddress);

    await this.rapToken.deployed();

    await this.users[0].sendTransaction({to: ADMIN_WALLET, value: tenEther}); // Send some funds to admin wallet
    await HRE.network.provider.request({method: 'hardhat_impersonateAccount', params: [ADMIN_WALLET]})
    this.admin = await ethers.provider.getSigner(ADMIN_WALLET);

    await this.rapToken.connect(this.admin).transfer(this.users[0].address, '10000' + DECIMAL_ZEROS);
    await this.rapToken.connect(this.admin).approve(panCakeV2RouterAddress, '40000000' + DECIMAL_ZEROS); // 40M to pancake router

    await this.panCakeRouter.connect(this.admin).addLiquidityETH(this.rapToken.address, '40000000' + DECIMAL_ZEROS, 0, 0, ADMIN_WALLET, new Date().getTime(), {
      value: ethers.utils.parseEther('40')
    }); // provide 40 BNB + 40M token liquidity to pancakeswap

    await this.rapToken.connect(this.admin).burn();
  });

  it("Should tax this tx", async function() {
    chai.expect(await this.rapToken.balanceOf(this.users[1].address)).to.be.equal('0'); // 0 tokens
    chai.expect(await this.rapToken.balanceOf(CHARITY_WALLET_1)).to.be.equal('0');
    chai.expect(await this.rapToken.balanceOf(CHARITY_WALLET_2)).to.be.equal('0');
    await this.rapToken.connect(this.admin).transfer(this.users[1].address, '10000' + DECIMAL_ZEROS);
    chai.expect(await this.rapToken.balanceOf(this.users[1].address)).to.be.equal('9500009500000000000000'); // 10000 - 500 (5% Tax) + 0.475(received in distribution) tokens
  });

  it("should tax and distribute to other users", async function () {
    chai.expect(await this.rapToken.balanceOf(BURN_ADDRESS)).to.be.equal('80000000' + DECIMAL_ZEROS);
    await this.rapToken.connect(this.admin).transfer(this.users[1].address, '10000' + DECIMAL_ZEROS);
    chai.expect(await this.rapToken.balanceOf(this.users[0].address)).to.be.equal('10000010000000000000000'); // 0.5 received through distribution
    chai.expect(await this.rapToken.balanceOf(BURN_ADDRESS)).to.be.equal('80000080000000000000000000');
  })

  it("should not allow transfer from and to zero address, also should not allow to transfer more then his balance", async function () {
    await chai.expect(
        this.rapToken.connect(this.admin).transfer(ZERO_ADDRESS, '100')
    ).to.be.revertedWith("ERC20: transfer to the zero address");

    await chai.expect(
        this.rapToken.connect(this.users[0]).transfer(ADMIN_WALLET, '100000' + DECIMAL_ZEROS)
    ).to.be.revertedWith("ERC20: transfer amount exceeds balance");

    // HH cannot impersonate 0x0 account @see => https://github.com/nomiclabs/hardhat/issues/1081
    //
    // await this.users[0].sendTransaction({to: ZERO_ADDRESS, value: ethers.utils.parseEther('10')}); // Send some funds to admin wallet
    // await HRE.network.provider.request({method: 'hardhat_impersonateAccount', params: [ZERO_ADDRESS]})
    // this.ZERO_ADDRESS = await ethers.provider.getSigner(ZERO_ADDRESS);
    //
    // await chai.expect(
    //     this.rapToken.connect(this.ZERO_ADDRESS).transfer(ADMIN_WALLET, '100')
    // ).to.be.revertedWith("ERC20: transfer from the zero address");
  })
});
