const HRE = require('hardhat');
const chai = require("chai");

const ADMIN_WALLET = "0x3c021c3999B9f011Ff5a48b2F4B3f1802401699F";
const BURN_ADDRESS = "0x000000000000000000000000000000000000dEaD";

const DECIMAL_ZEROS = "000000000000000000"; // 18 zeros

describe("RapToken without tax tests", function() {
  beforeEach(async function() {
    this.users = await ethers.getSigners();
    const tenEther = ethers.utils.parseEther('10');
    const RapToken = await ethers.getContractFactory("RapToken");
    this.rapToken = await RapToken.deploy();

    await this.rapToken.deployed();

    await this.users[0].sendTransaction({to: ADMIN_WALLET, value: tenEther}); // Send some funds to admin wallet
    await HRE.network.provider.request({method: 'hardhat_impersonateAccount', params: [ADMIN_WALLET]})
    this.admin = await ethers.provider.getSigner(ADMIN_WALLET);
  });

  it("Should distribute tokens to all accounts in correct proportion", async function() {
    chai.expect(await this.rapToken.balanceOf(ADMIN_WALLET)).to.be.equal('200000000' + DECIMAL_ZEROS); // 200M tokens
  });

  it("Should not tax before Burn event is over", async function() {
    const tokensToTransfer = 10000;
    await this.rapToken.connect(this.admin).transfer(this.users[0].address, tokensToTransfer + DECIMAL_ZEROS);
    chai.expect(await this.rapToken.balanceOf(this.users[0].address)).to.be.equal(tokensToTransfer + DECIMAL_ZEROS);
  });

  it("Should not allow burning from non admin", async function() {
    await chai.expect(
        this.rapToken.connect(this.users[0]).burn()
    ).to.be.revertedWith("Ownable: caller is not the owner");
  });

  it("Should allow burning after ICO is over", async function() {
    await chai.expect(
        this.rapToken.connect(this.admin).burn()
    ).to.emit(this.rapToken, 'Burned');
    chai.expect(await this.rapToken.balanceOf(ADMIN_WALLET)).to.be.equal('120000000' + DECIMAL_ZEROS); // 120M tokens, 80M burned
    chai.expect(await this.rapToken.balanceOf(BURN_ADDRESS)).to.be.equal('80000000' + DECIMAL_ZEROS); // 80M tokens

    // Should not allow burn event to happen twice
    await chai.expect(
        this.rapToken.connect(this.admin).burn()
    ).to.be.reverted;
  });
});
