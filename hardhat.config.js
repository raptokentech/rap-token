require('dotenv').config();
require("@nomiclabs/hardhat-waffle");
require("solidity-coverage");
require("hardhat-gas-reporter");
require('hardhat-deploy');


// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async () => {
  const accounts = await ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more
/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  defaultNetwork: "hardhat",
  solidity: {
    version: "0.8.0",
    settings: {
      optimizer: {
        enabled: false,
        runs: 200
      }
    }
  },
  gasReporter: {
    currency: 'USD',
    gasPrice: 20
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  },
  networks: {
    hardhat: {
      forking: {
        url: process.env.NODE_RPC_URL
      }
    },
    BSCTestnet: {
      url: process.env.NODE_RPC_URL,
      accounts: process.env.PRIVATE_KEY.split(','),
      networkCheckTimeout: 20000,
      skipDryRun: true,
      gas: 7000000,
      gasPrice: 25000000000,
      network_id: 97
    },
    BSCMainnet: {
      url: process.env.NODE_RPC_URL,
      accounts: process.env.PRIVATE_KEY.split(','),
      networkCheckTimeout: 20000,
      skipDryRun: true,
      gas: 7000000,
      gasPrice: 25000000000,
      network_id: 56
    },
  }
};

